package step7;

import io.vavr.control.Try;
import org.immutables.value.Value;

import java.util.HashMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.vavr.API.*;
import static io.vavr.Predicates.instanceOf;

class FeedHandler {
  private final Repository repository;

  FeedHandler(final Repository repository) {
    this.repository = repository;
  }

  List<Doc> handle(final List<Doc> changes, final Function<Doc, Try<Resource>> creator) {
    return changes.stream()
      .filter(this::isImportant)
      .map(doc ->
        creator
          .apply(doc)
          .recover(x -> Match(x).of(
            Case($(instanceOf(DuplicateResourceException.class)), t -> handleDuplicate(doc)),
            Case($(instanceOf(SpecialException.class)), FeedHandler::handleSpecial)))
          .map(resource -> setToProcessed(doc, resource))
          .getOrElseGet(e -> setToFailed(doc, e)))
      .collect(Collectors.toList());
  }

  private Resource handleDuplicate(final Doc alreadyProcessed) {
    return repository.findById(alreadyProcessed.apiId());
  }

  private static Resource handleSpecial(final SpecialException e) {
    return new Resource();
  }

  private Doc setToFailed(final Doc doc, final Throwable e) {
    return ImmutableDoc.copyOf(doc)
      .withStatus("failed")
      .withError(e.getMessage());
  }

  private Doc setToProcessed(final Doc doc, final Resource resource) {
    return ImmutableDoc.copyOf(doc)
      .withApiId(resource.get("id"))
      .withStatus("processed");
  }

  private boolean isImportant(final Doc doc) {
    return "important".equals(doc.type());
  }
}

@Value.Immutable
interface Doc {
  String title();

  String type();

  String apiId();

  String status();

  String error();

  class Builder extends ImmutableDoc.Builder {
  }
}

class Resource extends HashMap<String, String> {
}

interface Repository {
  Resource findById(String id);
}

class DuplicateResourceException extends RuntimeException {
}

class SpecialException extends RuntimeException {
}
