package step6;

import org.immutables.value.Value;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

class FeedHandler {
  CompletableFuture<List<Doc>> handle(final List<Doc> changes, final CompletableFuture<Resource> creator) {
    final List<CompletableFuture<Doc>> futures = changes.stream()
      .filter(this::isImportant)
      .map(doc ->
        creator
          .thenApply(resource -> setToProcessed(doc, resource))
          .exceptionally(e -> setToFailed(doc, e)))
      .collect(Collectors.toList());
    return CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]))
      .thenApply(v ->
        futures.stream()
          .map(CompletableFuture::join)
          .collect(Collectors.toList()));
  }

  private Doc setToFailed(final Doc doc, final Throwable e) {
    return ImmutableDoc.copyOf(doc)
      .withStatus("failed")
      .withError(e.getMessage());
  }

  private Doc setToProcessed(final Doc doc, final Resource resource) {
    return ImmutableDoc.copyOf(doc)
      .withApiId(resource.get("id"))
      .withStatus("processed");
  }

  private boolean isImportant(final Doc doc) {
    return "important".equals(doc.type());
  }
}

@Value.Immutable
interface Doc {
  String title();

  String type();

  String apiId();

  String status();

  String error();

  class Builder extends ImmutableDoc.Builder {
  }
}

class Resource extends HashMap<String, String> {
}


