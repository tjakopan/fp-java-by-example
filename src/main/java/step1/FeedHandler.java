package step1;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

class FeedHandler {
  private final WebService webService;
  private final DocumentDb documentDb;

  FeedHandler(final WebService webService, final DocumentDb documentDb) {
    this.webService = webService;
    this.documentDb = documentDb;
  }

  void handle(final List<Doc> changes) {
    changes.stream()
      .filter(doc -> "important".equals(doc.getType()))
      .forEach(doc -> {
        try {
          final Resource resource = webService.create(doc);
          doc.setApiId(resource.get("id"));
          doc.setStatus("processed");
        } catch (final IOException e) {
          doc.setStatus("failed");
          doc.setError(e.getMessage());
        }
        try {
          documentDb.update(doc);
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      });
  }
}

class Doc {
  private String title;
  private String type;
  private String apiId;
  private String status;
  private String error;

  public Doc(final String title, final String type, final String apiId, final String status, final String error) {
    this.title = title;
    this.type = type;
    this.apiId = apiId;
    this.status = status;
    this.error = error;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getType() {
    return type;
  }

  public void setType(final String type) {
    this.type = type;
  }

  public String getApiId() {
    return apiId;
  }

  public void setApiId(final String apiId) {
    this.apiId = apiId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(final String status) {
    this.status = status;
  }

  public String getError() {
    return error;
  }

  public void setError(final String error) {
    this.error = error;
  }
}

class Resource extends HashMap<String, String> {
}

interface WebService {
  Resource create(Doc doc) throws IOException;
}

interface DocumentDb {
  void update(Object obj) throws IOException;
}


