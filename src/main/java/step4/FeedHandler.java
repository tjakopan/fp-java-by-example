package step4;

import org.immutables.value.Value;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class FeedHandler {
  private final WebService webService;
  private final DocumentDb documentDb;

  FeedHandler(final WebService webService, final DocumentDb documentDb) {
    this.webService = webService;
    this.documentDb = documentDb;
  }

  CompletableFuture<Void> handle(final List<Doc> changes) {
    final CompletableFuture[] futures = changes.stream()
      .filter(this::isImportant)
      .map(doc ->
        createResource(doc)
          .thenCompose(resource -> documentDb.update(setToProcessed(doc, resource)))
          .handle((v, e) -> e == null ? CompletableFuture.completedFuture(v) : documentDb.update(setToFailed(doc, e)))
          .thenCompose(v -> v))
      .toArray(CompletableFuture[]::new);
    return CompletableFuture.allOf(futures);
  }

  private Doc setToFailed(final Doc doc, final Throwable e) {
    return ImmutableDoc.copyOf(doc)
      .withStatus("failed")
      .withError(e.getMessage());
  }

  private Doc setToProcessed(final Doc doc, final Resource resource) {
    return ImmutableDoc.copyOf(doc)
      .withApiId(resource.get("id"))
      .withStatus("processed");
  }

  private CompletableFuture<Resource> createResource(final Doc doc) {
    return webService.create(doc);
  }

  private boolean isImportant(final Doc doc) {
    return "important".equals(doc.type());
  }
}

@Value.Immutable
interface Doc {
  String title();

  String type();

  String apiId();

  String status();

  String error();

  class Builder extends ImmutableDoc.Builder {
  }
}

class Resource extends HashMap<String, String> {
}

interface WebService {
  CompletableFuture<Resource> create(Doc doc);
}

interface DocumentDb {
  CompletableFuture<Void> update(Object obj);
}


