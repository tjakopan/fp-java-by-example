package step3;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class FeedHandler {
  private final WebService webService;
  private final DocumentDb documentDb;

  FeedHandler(final WebService webService, final DocumentDb documentDb) {
    this.webService = webService;
    this.documentDb = documentDb;
  }

  CompletableFuture<Void> handle(final List<Doc> changes) {
    final CompletableFuture[] futures = changes.stream()
      .filter(this::isImportant)
      .map(doc ->
        createResource(doc)
          .thenCompose(resource -> updateToProcessed(doc, resource))
          .handle((v, e) -> e == null ? CompletableFuture.completedFuture(v) : updateToFailed(doc, e))
          .thenCompose(v -> v))
      .toArray(CompletableFuture[]::new);
    return CompletableFuture.allOf(futures);
  }

  private CompletableFuture<Void> updateToFailed(final Doc doc, final Throwable e) {
    doc.setStatus("failed");
    doc.setError(e.getMessage());
    return documentDb.update(doc);
  }

  private CompletableFuture<Void> updateToProcessed(final Doc doc, final Resource resource) {
    doc.setApiId(resource.get("id"));
    doc.setStatus("processed");
    return documentDb.update(doc);
  }

  private CompletableFuture<Resource> createResource(final Doc doc) {
    return webService.create(doc);
  }

  private boolean isImportant(final Doc doc) {
    return "important".equals(doc.getType());
  }
}

class Doc {
  private String title;
  private String type;
  private String apiId;
  private String status;
  private String error;

  public Doc(final String title, final String type, final String apiId, final String status, final String error) {
    this.title = title;
    this.type = type;
    this.apiId = apiId;
    this.status = status;
    this.error = error;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getType() {
    return type;
  }

  public void setType(final String type) {
    this.type = type;
  }

  public String getApiId() {
    return apiId;
  }

  public void setApiId(final String apiId) {
    this.apiId = apiId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(final String status) {
    this.status = status;
  }

  public String getError() {
    return error;
  }

  public void setError(final String error) {
    this.error = error;
  }
}

class Resource extends HashMap<String, String> {
}

interface WebService {
  CompletableFuture<Resource> create(Doc doc);
}

interface DocumentDb {
  CompletableFuture<Void> update(Object obj);
}


