package step6;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.immutables.value.Generated;

/**
 * Immutable implementation of {@link Doc}.
 * <p>
 * Use the builder to create immutable instances:
 * {@code new Doc.Builder()}.
 */
@Generated(from = "Doc", generator = "Immutables")
@SuppressWarnings({"all"})
@javax.annotation.Generated("org.immutables.processor.ProxyProcessor")
final class ImmutableDoc implements Doc {
  private final String title;
  private final String type;
  private final String apiId;
  private final String status;
  private final String error;

  private ImmutableDoc(
      String title,
      String type,
      String apiId,
      String status,
      String error) {
    this.title = title;
    this.type = type;
    this.apiId = apiId;
    this.status = status;
    this.error = error;
  }

  /**
   * @return The value of the {@code title} attribute
   */
  @Override
  public String title() {
    return title;
  }

  /**
   * @return The value of the {@code type} attribute
   */
  @Override
  public String type() {
    return type;
  }

  /**
   * @return The value of the {@code apiId} attribute
   */
  @Override
  public String apiId() {
    return apiId;
  }

  /**
   * @return The value of the {@code status} attribute
   */
  @Override
  public String status() {
    return status;
  }

  /**
   * @return The value of the {@code error} attribute
   */
  @Override
  public String error() {
    return error;
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Doc#title() title} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for title
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableDoc withTitle(String value) {
    String newValue = Objects.requireNonNull(value, "title");
    if (this.title.equals(newValue)) return this;
    return new ImmutableDoc(newValue, this.type, this.apiId, this.status, this.error);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Doc#type() type} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for type
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableDoc withType(String value) {
    String newValue = Objects.requireNonNull(value, "type");
    if (this.type.equals(newValue)) return this;
    return new ImmutableDoc(this.title, newValue, this.apiId, this.status, this.error);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Doc#apiId() apiId} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for apiId
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableDoc withApiId(String value) {
    String newValue = Objects.requireNonNull(value, "apiId");
    if (this.apiId.equals(newValue)) return this;
    return new ImmutableDoc(this.title, this.type, newValue, this.status, this.error);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Doc#status() status} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for status
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableDoc withStatus(String value) {
    String newValue = Objects.requireNonNull(value, "status");
    if (this.status.equals(newValue)) return this;
    return new ImmutableDoc(this.title, this.type, this.apiId, newValue, this.error);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Doc#error() error} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for error
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableDoc withError(String value) {
    String newValue = Objects.requireNonNull(value, "error");
    if (this.error.equals(newValue)) return this;
    return new ImmutableDoc(this.title, this.type, this.apiId, this.status, newValue);
  }

  /**
   * This instance is equal to all instances of {@code ImmutableDoc} that have equal attribute values.
   * @return {@code true} if {@code this} is equal to {@code another} instance
   */
  @Override
  public boolean equals(Object another) {
    if (this == another) return true;
    return another instanceof ImmutableDoc
        && equalTo((ImmutableDoc) another);
  }

  private boolean equalTo(ImmutableDoc another) {
    return title.equals(another.title)
        && type.equals(another.type)
        && apiId.equals(another.apiId)
        && status.equals(another.status)
        && error.equals(another.error);
  }

  /**
   * Computes a hash code from attributes: {@code title}, {@code type}, {@code apiId}, {@code status}, {@code error}.
   * @return hashCode value
   */
  @Override
  public int hashCode() {
    int h = 5381;
    h += (h << 5) + title.hashCode();
    h += (h << 5) + type.hashCode();
    h += (h << 5) + apiId.hashCode();
    h += (h << 5) + status.hashCode();
    h += (h << 5) + error.hashCode();
    return h;
  }

  /**
   * Prints the immutable value {@code Doc} with attribute values.
   * @return A string representation of the value
   */
  @Override
  public String toString() {
    return "Doc{"
        + "title=" + title
        + ", type=" + type
        + ", apiId=" + apiId
        + ", status=" + status
        + ", error=" + error
        + "}";
  }

  /**
   * Creates an immutable copy of a {@link Doc} value.
   * Uses accessors to get values to initialize the new immutable instance.
   * If an instance is already immutable, it is returned as is.
   * @param instance The instance to copy
   * @return A copied immutable Doc instance
   */
  public static ImmutableDoc copyOf(Doc instance) {
    if (instance instanceof ImmutableDoc) {
      return (ImmutableDoc) instance;
    }
    return new Doc.Builder()
        .from(instance)
        .build();
  }

  /**
   * Builds instances of type {@link ImmutableDoc ImmutableDoc}.
   * Initialize attributes and then invoke the {@link #build()} method to create an
   * immutable instance.
   * <p><em>{@code Builder} is not thread-safe and generally should not be stored in a field or collection,
   * but instead used immediately to create instances.</em>
   */
  @Generated(from = "Doc", generator = "Immutables")
  public static class Builder {
    private static final long INIT_BIT_TITLE = 0x1L;
    private static final long INIT_BIT_TYPE = 0x2L;
    private static final long INIT_BIT_API_ID = 0x4L;
    private static final long INIT_BIT_STATUS = 0x8L;
    private static final long INIT_BIT_ERROR = 0x10L;
    private long initBits = 0x1fL;

    private String title;
    private String type;
    private String apiId;
    private String status;
    private String error;

    /**
     * Creates a builder for {@link ImmutableDoc ImmutableDoc} instances.
     * <pre>
     * new Doc.Builder()
     *    .title(String) // required {@link Doc#title() title}
     *    .type(String) // required {@link Doc#type() type}
     *    .apiId(String) // required {@link Doc#apiId() apiId}
     *    .status(String) // required {@link Doc#status() status}
     *    .error(String) // required {@link Doc#error() error}
     *    .build();
     * </pre>
     */
    public Builder() {
      if (!(this instanceof Doc.Builder)) {
        throw new UnsupportedOperationException("Use: new Doc.Builder()");
      }
    }

    /**
     * Fill a builder with attribute values from the provided {@code Doc} instance.
     * Regular attribute values will be replaced with those from the given instance.
     * Absent optional values will not replace present values.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Doc.Builder from(Doc instance) {
      Objects.requireNonNull(instance, "instance");
      title(instance.title());
      type(instance.type());
      apiId(instance.apiId());
      status(instance.status());
      error(instance.error());
      return (Doc.Builder) this;
    }

    /**
     * Initializes the value for the {@link Doc#title() title} attribute.
     * @param title The value for title 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Doc.Builder title(String title) {
      this.title = Objects.requireNonNull(title, "title");
      initBits &= ~INIT_BIT_TITLE;
      return (Doc.Builder) this;
    }

    /**
     * Initializes the value for the {@link Doc#type() type} attribute.
     * @param type The value for type 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Doc.Builder type(String type) {
      this.type = Objects.requireNonNull(type, "type");
      initBits &= ~INIT_BIT_TYPE;
      return (Doc.Builder) this;
    }

    /**
     * Initializes the value for the {@link Doc#apiId() apiId} attribute.
     * @param apiId The value for apiId 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Doc.Builder apiId(String apiId) {
      this.apiId = Objects.requireNonNull(apiId, "apiId");
      initBits &= ~INIT_BIT_API_ID;
      return (Doc.Builder) this;
    }

    /**
     * Initializes the value for the {@link Doc#status() status} attribute.
     * @param status The value for status 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Doc.Builder status(String status) {
      this.status = Objects.requireNonNull(status, "status");
      initBits &= ~INIT_BIT_STATUS;
      return (Doc.Builder) this;
    }

    /**
     * Initializes the value for the {@link Doc#error() error} attribute.
     * @param error The value for error 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Doc.Builder error(String error) {
      this.error = Objects.requireNonNull(error, "error");
      initBits &= ~INIT_BIT_ERROR;
      return (Doc.Builder) this;
    }

    /**
     * Builds a new {@link ImmutableDoc ImmutableDoc}.
     * @return An immutable instance of Doc
     * @throws java.lang.IllegalStateException if any required attributes are missing
     */
    public ImmutableDoc build() {
      if (initBits != 0) {
        throw new IllegalStateException(formatRequiredAttributesMessage());
      }
      return new ImmutableDoc(title, type, apiId, status, error);
    }

    private String formatRequiredAttributesMessage() {
      List<String> attributes = new ArrayList<>();
      if ((initBits & INIT_BIT_TITLE) != 0) attributes.add("title");
      if ((initBits & INIT_BIT_TYPE) != 0) attributes.add("type");
      if ((initBits & INIT_BIT_API_ID) != 0) attributes.add("apiId");
      if ((initBits & INIT_BIT_STATUS) != 0) attributes.add("status");
      if ((initBits & INIT_BIT_ERROR) != 0) attributes.add("error");
      return "Cannot build Doc, some of required attributes are not set " + attributes;
    }
  }
}
